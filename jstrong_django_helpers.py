def display_obj_attrs(obj, ignore_fields):
	"display every attribute other than fields listed in list ignore_fields"
	for attribute in obj._meta.get_all_field_names():
            if attribute not in ignore_fields:
                spaces = 30-len(attribute)
                print attribute + ':',' ' * spaces, getattr(obj,attribute)